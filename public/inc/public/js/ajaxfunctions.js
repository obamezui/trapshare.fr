/* 
 * les différentes 
 * fonctions ajax 
 */


$(document).ready(function () {

    var departquery = function (search) {
        $.ajax({
            url: "/account/api/getairport",
            type: "POST",
            data: search,
            dataType: 'json',
            success: function (data) {
                $("#depart").typeahead({source: data, items: "all",autoSelect:true});
            },
            error: function (exception) {

                console.log(exception);

            }
        });



    }

    var destinationquery = function (search) {
        $.ajax({
            url: "/account/api/getairport",
            type: "POST",
            data: search,
            dataType: 'json',
            success: function (data) {
                $("#destination").typeahead({source: data, items: "all",autoSelect:true});
            },
            error: function (exception) {

                console.log(exception);

            }
        });



    }


    $("#depart").on('keyup', function () {
        var searchbox = $(this).val();
        var dataString = 'search=' + searchbox;
        if (searchbox == '') {
            //si on a rien taper on ne fait rien 
        } else
        {

            departquery(dataString);
        }



    });

    $("#destination").on('keyup', function () {
        var searchbox = $(this).val();
        var dataString = 'search=' + searchbox;
        if (searchbox == '') {
            //si on a rien taper on ne fait rien 
        } else
        {

            destinationquery(dataString);
        }



    });
});
