<?php

class Account_IndexController extends Zend_Controller_Action
{

      public function init()
    {
        /* Initialize action controller here */
        
        /**
         * pour desactiver le layout
         * $this->_helper->layout()->disableLayout();
         */
        /**
         * on test si l'utilisateur des déja connecté
         */
      
        $this->oSessionSecurite = new Zend_Session_Namespace('securite');
           $this-> _testConnexion();
           $this->_setLayout();
           if(isset($this->oSessionSecurite->userinfo)){
            
              $this->oUtilisateur = new Public_Models_DbTable_Utilisateur();
         $this->oUtilinfos = $this->oUtilisateur->recupererinfos($this->oSessionSecurite->userinfo['id']);
        $this->view->oUtilisateur = $this->oUtilinfos;
        $this->view->nom = $this->oSessionSecurite->userinfo['nom']." ". $this->oSessionSecurite->userinfo['prenom'];
        }
          
           
    }

    public function indexAction()
    {
        
        
    }


    public function loginAction()
    {
        // on initialise le formulaire
        $loginForm = new Public_Forms_Login();
        // on transmet le formulaire à la vue
         $this->view->loginForm = $loginForm; 
           if ($this->getRequest()->isPost()) {
            //Récupération des donnnées
            $oDonnees = $this->getRequest()->getPost();
            if ($loginForm->isValid($oDonnees)) {
               //Déclencher l'ajout des données 
                $this->oModeleUtilisateur = new Public_Models_DbTable_Utilisateur();
               
              $rs =   $this->oModeleUtilisateur->recuperer($loginForm->getValue("login"),$loginForm->getValue("mdp"));
             
              if (!$rs) {
                  //1= mauvais identifiant ou mot de passe 
                  $this->view->resultat = 1;
              }  else {
                  $this->oSessionSecurite->userinfo = $rs;
                  //identification accepté
                   $this->view->resultat = 2;
                   $this->view->oForm ="";
                   //Initialisation de la session 
                  // $oSessionSecurite = new Zend_Session_Namespace('securite');
                   $this->oSessionSecurite->laisserPasser =true;
                   $this->_redirect("/account/index");
                // $this->_redirect("/cockpit/index");
            
              }
               
            }
        }
        $this-> _testConnexion();
    }

   
   

    private function _testConnexion()
    {
         if ($this->oSessionSecurite->laisserPasser=="ok") {
             $this->view->log = true;
         }  else {
             $this->view->log = false;
         }
        
    }
    
     private function _setLayout()
    {
         if ( $this->oSessionSecurite->laisserPasser =="ok") {
               $this->_helper->layout->setLayout('account');
           }else{
                $this->_helper->layout->setLayout('login');
                
           }
        
    }
    
   

    public function logoutAction()
    {
        // fonction de déconnexion
            unset($this->oSessionSecurite->laisserPasser);
            unset($this->oSessionSecurite->userinfo);
            $this-> _testConnexion();
            $this->_redirect("/account/index");
    }


}

