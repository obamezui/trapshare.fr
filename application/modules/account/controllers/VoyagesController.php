<?php

class Account_VoyagesController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_helper->layout->setLayout('account');
        $this->oSessionSecurite = new Zend_Session_Namespace('securite');
        $this->oUtilisateur = new Public_Models_DbTable_Utilisateur();
        $this->oUtilinfos = $this->oUtilisateur->recupererinfos($this->oSessionSecurite->userinfo['id']);
        $this->view->oUtilisateur = $this->oUtilinfos;
        $this->view->nom = $this->oSessionSecurite->userinfo['nom'] . " " . $this->oSessionSecurite->userinfo['prenom'];
    }

    public function indexAction()
    {
        $this->oModele = new Account_Models_DbTable_Voyages();
        $this->view->volInfos = $this->oModele->recupererTous($this->oUtilinfos["id"]);
    }

    public function ajouterAction()
    {
        $formvoyage = new Account_Forms_Voyages();
        $this->view->voyageform = $formvoyage;
        if ($this->getRequest()->isPost()) {
            //Récupération des donnnées

            $oDonnees = $this->getRequest()->getPost();
            if ($formvoyage->isValid($oDonnees)) {
                //Déclencher l'ajout des données 
                $this->oModele = new Account_Models_DbTable_Voyages();
                $id = $this->oUtilinfos["id"];
                $date = explode('/', $formvoyage->getValue("date"));
                $jour = $date[0];
                $mois = $date[1];
                $annee = $date[2];

                $this->oModele->ajouter(array(
                    "id" => new Zend_Db_Expr('UUID()'),
                    "id_util" => $id,
                    "heure" => $formvoyage->getValue("heure"),
                    "jour" => $jour,
                    "mois" => $mois,
                    "annee" => $annee,
                    "texte" => $formvoyage->getValue("texte"),
                    "kilo" => $formvoyage->getValue("kilo"),
                    "prix" => $formvoyage->getValue("prix"),
                    "depart" => $formvoyage->getValue("depart"),
                    "destination" => $formvoyage->getValue("destination")
                ));
            } else {
                $formvoyage->populate($oDonnees);
            }
        }
    }

    public function editerAction()
    {
         $formvoyage = new Account_Forms_Voyages();
          $this->oModele = new Account_Models_DbTable_Voyages();
          $this->idVyges = $this->getParam('id',0);
         $voyages =  $this->oModele->recuperer($this->getParam('id',0),$this->oUtilinfos["id"]);
         $formvoyage->populate(
             array(
                         "depart" =>$voyages->depart,
                        "destination" => $voyages->destination,
                        "heure" => $voyages->heure,
                        "date" => $voyages->jour.'/'.$voyages->mois.'/'.$voyages->annee,
                        "kilo" => $voyages->kilo,
                        "prix" => $voyages->prix,
                        "texte" => $voyages->texte
                            )    
                 );
        $this->view->voyageform = $formvoyage;
        $this->view->prix = $voyages->prix;
                $this->view->kilo = $voyages->kilo;
                
        if ($this->getRequest()->isPost()) {
            //Récupération des donnnées

            $oDonnees = $this->getRequest()->getPost();
            if ($formvoyage->isValid($oDonnees)) {
                //Déclencher l'ajout des données 
                $this->oModele = new Account_Models_DbTable_Voyages();
                $id = $this->oUtilinfos["id"];
                $date = explode('/', $formvoyage->getValue("date"));
                $jour = $date[0];
                $mois = $date[1];
                $annee = $date[2];

                $this->oModele->modifier(array(
                    "id_util" => $id,
                    "heure" => $formvoyage->getValue("heure"),
                    "jour" => $jour,
                    "mois" => $mois,
                    "annee" => $annee,
                    "texte" => $formvoyage->getValue("texte"),
                    "kilo" => $formvoyage->getValue("kilo"),
                    "prix" => $formvoyage->getValue("prix"),
                    "depart" => $formvoyage->getValue("depart"),
                    "destination" => $formvoyage->getValue("destination")
                ), $this->idVyges);
            } else {
                $formvoyage->populate($oDonnees);
            }
        }
    }

    public function supprimerAction()
    {
        $formvoyage = new Account_Forms_Voyages();
          $this->oModele = new Account_Models_DbTable_Voyages();
          $this->idVyges = $this->getParam('id',0);
         $voyages =  $this->oModele->supprimer($this->getParam('id',0));
         $this->_redirect("/account/voyages");
    }


}




