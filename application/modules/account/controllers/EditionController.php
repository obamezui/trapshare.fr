<?php

class Account_EditionController extends Zend_Controller_Action {

    public function init() {
        $this->_helper->layout->setLayout('account');
        $this->oSessionSecurite = new Zend_Session_Namespace('securite');
        $this->oUtilisateur = new Public_Models_DbTable_Utilisateur();
         $this->oUtilinfos = $this->oUtilisateur->recupererinfos($this->oSessionSecurite->userinfo['id']);
        $this->view->oUtilisateur = $this->oUtilinfos;
        $this->view->nom = $this->oSessionSecurite->userinfo['nom']." ". $this->oSessionSecurite->userinfo['prenom'];
    }

    public function indexAction() {

        $formEdition = new Account_Forms_Editionprofil();
       
        $formEdition->populate(
            array(
                         "nom" =>$this->oUtilinfos["nom"],
                        "prenom" => $this->oUtilinfos["prenom"],
                        "pseudo" => $this->oUtilinfos["pseudo"],
                        "addr1" => $this->oUtilinfos["adresse1"],
                        "addr2" => $this->oUtilinfos["adresse2"],
                        "mob" => $this->oUtilinfos["mobile"],
                        "fixe" => $this->oUtilinfos["fixe"],
                        "mail" => $this->oUtilinfos["email"],
                        "ville" => $this->oUtilinfos["ville"],
                        "bp" => $this->oUtilinfos["bp"]
                            )           
           );
        $this->view->formEdition = $formEdition;
        if ($this->getRequest()->isPost()) {
            //Récupération des donnnées

            $oDonnees = $this->getRequest()->getPost();
            if ($formEdition->isValid($oDonnees)) {
                //Déclencher l'ajout des données 
                $this->oModele = new Account_Models_DbTable_Imageprofil();
                $id = $this->oUtilinfos["id"];
                $uploadedData = $formEdition->getValues();
                
               // $img=$uploadedData["picture"];
                $mdp= $this->oUtilinfos["mdp"];
                if(strlen(trim($formEdition->getValue("mdp")))>0){
                    
                    $mdp= md5($formEdition->getValue("mdp"));
                }
                if ($formEdition->picture->isUploaded()) {
                    //on recupere les infos sur le fichier envoyé
                    $img = pathinfo($formEdition->picture->getFileName())["filename"].'.jpg';
                    $this->oModele->modifier(array(
                        "nom" => $formEdition->getValue("nom"),
                        "prenom" => $formEdition->getValue("prenom"),
                        "pseudo" => $formEdition->getValue("pseudo"),
                        "mdp" => $mdp,
                        "adresse1" => $formEdition->getValue("addr1"),
                        "adresse2" => $formEdition->getValue("addr2"),
                        "mobile" => $formEdition->getValue("mob"),
                        "fixe" => $formEdition->getValue("fixe"),
                        "email" => $formEdition->getValue("mail"),
                        "ville" => $formEdition->getValue("ville"),
                        "bp" => $formEdition->getValue("bp"),
                        "picture" => $img
                            ), $id);
                } else {
                    $this->oModele->modifier(array(
                         "nom" => $formEdition->getValue("nom"),
                        "prenom" => $formEdition->getValue("prenom"),
                        "pseudo" => $formEdition->getValue("pseudo"),
                        "mdp" => $mdp,
                        "adresse1" => $formEdition->getValue("addr1"),
                        "adresse2" => $formEdition->getValue("addr2"),
                        "mobile" => $formEdition->getValue("mob"),
                        "fixe" => $formEdition->getValue("fixe"),
                        "email" => $formEdition->getValue("mail"),
                        "ville" => $formEdition->getValue("ville"),
                        "bp" => $formEdition->getValue("bp")
                            ), $id);
                }
              $this->oUtilinfos = $this->oUtilisateur->recupererinfos($this->oSessionSecurite->userinfo['id']);
            $this->view->oUtilisateur = $this->oUtilinfos;
            } else {
                $formEdition->populate($oDonnees);
            }
        }
    }

    public function imageprofilAction() {
        
    }

}
