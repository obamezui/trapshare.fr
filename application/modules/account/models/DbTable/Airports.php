<?php

class Account_Models_DbTable_Airports extends Zend_Db_Table_Abstract {

    protected $_name = 'airports';
    protected $_primary = 'code';

    public function recuperer($airportname) {
        $select = $this->select()
                ->distinct()
                ->from($this, array('code', 'name', 'cityName', 'countryName'))
                ->where('cityName like?', '%'.$airportname .'%')
               ->order(array('name ASC'));
        $row = $this->fetchAll($select);
        return $row;
        
    }

}
