<?php

class Account_Models_DbTable_Voyages extends Zend_Db_Table_Abstract
{

    protected $_name = 'voyages';
     public function ajouter($tripData){
        $this->insert($tripData);
        
    }
    
     
       public function recuperer($tripId,$id_util){
        
         $select = $this->select()
                ->distinct()
                ->from($this)
                ->where('id = ?',$tripId)
                 ->where('id_util = ?',$id_util);
        $row = $this->fetchRow($select);
        return $row;
          
    }
    
    
      public function recupererVoyages($depart,$arrivee){
        
         $select = $this->select()
                ->distinct()
                ->from($this)
                ->where('depart like?', '%'.$depart .'%')
                ->where('destination like?', '%'.$arrivee .'%');
        $row = $this->fetchAll($select);
        return $row;
          
    }
    
     public function recupererTous($id_util){
        return $this->fetchAll(array("id_util=?"=>$id_util));
          
    }
    
      public function supprimer ($tripId){
        $this->delete('id='."'".$tripId."'");
        
    }

    public function modifier($tripData,$tripId){
        $this->update($tripData,'id='."'".$tripId."'");
        
    }

}

