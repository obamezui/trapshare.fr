<?php

class Account_Forms_Editionprofil extends Zend_Form {

    public function init() {
        /**
         * creation du formulaire 
         */
        /* on nomme le formulaire */
        
        $this->setMethod('post');
        $this->setName("inscription :");
        $this->setAttrib("class", "form-horizontal");
        // Création des éléments du formulaire 
        $nom = new Zend_Form_Element_Text("nom");
        $nom->setLabel('nom *:')
                ->setRequired(true)
                ->setFilters(array("StripTags", "StringTrim"))
                ->addValidator("notEmpty")
                ->setAttrib("placeholder", "votre nom")
                ->addErrorMessage("Ce champ est obligatoire")
                ->setAttrib("class", "form-control");
        $nom->setDecorators(array(
            'Errors',
            'ViewHelper',
            array(array('wrapperField' => 'HtmlTag'), array('tag' => 'div', 'class' => 'col-lg-6')),
            array('Label', array('placement' => 'prepend', 'class' => 'col-lg-2 control-label')),
            array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
        ));
        $prenom = new Zend_Form_Element_Text("prenom");
        $prenom->setLabel('prenom *:')
                ->setRequired(true)
                ->setFilters(array("StripTags", "StringTrim"))
                ->addValidator("notEmpty")
                ->setAttrib("placeholder", "votre prenom")
                ->addErrorMessage("Ce champ est obligatoire")
                ->setAttrib("class", "form-control");
        $prenom->setDecorators(array(
            'Errors',
            'ViewHelper',
            array(array('wrapperField' => 'HtmlTag'), array('tag' => 'div', 'class' => 'col-lg-6')),
            array('Label', array('placement' => 'prepend', 'class' => 'col-lg-2 control-label')),
            array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
        ));
        $pseudo = new Zend_Form_Element_Text("pseudo");
        $pseudo->setLabel('pseudo *:')
                ->setRequired(true)
                ->setFilters(array("StripTags", "StringTrim"))
                ->addValidator("notEmpty")
                ->setAttrib("placeholder", "votre pseudo")
                ->addErrorMessage("Ce champ est obligatoire")
                ->setAttrib("class", "form-control");
        $pseudo->setDecorators(array(
            'Errors',
            'ViewHelper',
            array(array('wrapperField' => 'HtmlTag'), array('tag' => 'div', 'class' => 'col-lg-6')),
            array('Label', array('placement' => 'prepend', 'class' => 'col-lg-2 control-label')),
            array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
        ));
        $mail = new Zend_Form_Element_Text("mail");
        $mail->setLabel('mail *:')
                ->setRequired(true)
                ->setFilters(array("StripTags", "StringTrim"))
                ->addValidator("notEmpty")
                ->setAttrib("placeholder", "votre mail")
                ->addErrorMessage("Ce champ est obligatoire")
                ->setAttrib("class", "form-control");
        $mail->setDecorators(array(
            'Errors',
            'ViewHelper',
            array(array('wrapperField' => 'HtmlTag'), array('tag' => 'div', 'class' => 'col-lg-6')),
            array('Label', array('placement' => 'prepend', 'class' => 'col-lg-2 control-label')),
            array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
        ));

        $addr1 = new Zend_Form_Element_Text("addr1");
        $addr1->setLabel('addresse 1 *:')
                ->setRequired(true)
                ->setFilters(array("StripTags", "StringTrim"))
                ->addValidator("notEmpty")
                ->setAttrib("placeholder", "address 1")
                ->addErrorMessage("Ce champ est obligatoire")
                ->setAttrib("class", "form-control");
        $addr1->setDecorators(array(
            'Errors',
            'ViewHelper',
            array(array('wrapperField' => 'HtmlTag'), array('tag' => 'div', 'class' => 'col-lg-6')),
            array('Label', array('placement' => 'prepend', 'class' => 'col-lg-2 control-label')),
            array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
        ));
        $addr2 = new Zend_Form_Element_Text("addr2");
        $addr2->setLabel('addresse 2 :')
                ->setAttrib("class", "form-control");
        $addr2->setDecorators(array(
            'Errors',
            'ViewHelper',
            array(array('wrapperField' => 'HtmlTag'), array('tag' => 'div', 'class' => 'col-lg-6')),
            array('Label', array('placement' => 'prepend', 'class' => 'col-lg-2 control-label')),
            array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
        ));
        $mob = new Zend_Form_Element_Text("mob");
        $mob->setLabel('numero mobile :')
                ->setRequired(true)
                ->setFilters(array("StripTags", "StringTrim"))
                ->addValidator("notEmpty")
                ->setAttrib("placeholder", "votre numero mobile")
                ->addErrorMessage("Ce champ est obligatoire")
                ->setAttrib("class", "form-control");
        $mob->setDecorators(array(
            'Errors',
            'ViewHelper',
            array(array('wrapperField' => 'HtmlTag'), array('tag' => 'div', 'class' => 'col-lg-6')),
            array('Label', array('placement' => 'prepend', 'class' => 'col-lg-2 control-label')),
            array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
        ));
        $fixe = new Zend_Form_Element_Text("fixe");
        $fixe->setLabel('numero fixe  :')
                ->setAttrib("class", "form-control");
        $fixe->setDecorators(array(
            'Errors',
            'ViewHelper',
            array(array('wrapperField' => 'HtmlTag'), array('tag' => 'div', 'class' => 'col-lg-6')),
            array('Label', array('placement' => 'prepend', 'class' => 'col-lg-2 control-label')),
            array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
        ));

        $mdp = new Zend_Form_Element_Password("mdp");
        $mdp->setLabel('mot de passe *:')
                ->setFilters(array("StripTags", "StringTrim"))
                ->setAttrib("placeholder", "Votre mot de passe")
                ->addErrorMessage("veuillez entrer un mot de passe valide")
                ->setAttrib("class", "form-control");
        $mdp->setDecorators(array(
            'Errors',
            'ViewHelper',
            array(array('wrapperField' => 'HtmlTag'), array('tag' => 'div', 'class' => 'col-lg-6')),
            array('Label', array('placement' => 'prepend', 'class' => 'col-lg-2 control-label')),
            array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
        ));
        $ville = new Zend_Form_Element_Text("ville");
        $ville->setLabel('ville *:')
                ->setRequired(true)
                ->setFilters(array("StripTags", "StringTrim"))
                ->addValidator("notEmpty")
                ->setAttrib("placeholder", "votre ville")
                ->addErrorMessage("Ce champ est obligatoire")
                ->setAttrib("class", "form-control");
        $ville->setDecorators(array(
            'Errors',
            'ViewHelper',
            array(array('wrapperField' => 'HtmlTag'), array('tag' => 'div', 'class' => 'col-lg-6')),
            array('Label', array('placement' => 'prepend', 'class' => 'col-lg-2 control-label')),
            array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
        ));
        $bp = new Zend_Form_Element_Text("bp");
        $bp->setLabel('Boite Postale *:')
                ->setRequired(true)
                ->setFilters(array("StripTags", "StringTrim"))
                ->addValidator("notEmpty")
                ->setAttrib("placeholder", "votre boite postale")
                ->addErrorMessage("Ce champ est obligatoire")
                ->setAttrib("class", "form-control");
        $bp->setDecorators(array(
            'Errors',
            'ViewHelper',
            array(array('wrapperField' => 'HtmlTag'), array('tag' => 'div', 'class' => 'col-lg-6')),
            array('Label', array('placement' => 'prepend', 'class' => 'col-lg-2 control-label')),
            array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
        ));
           $image = new Zend_Form_Element_File('picture');
          $image->setLabel('Image de profil:')
          ->setDestination('../public/inc/account/images/profil/'.$this->getView()->oUtilisateur['pseudo'])
          // Fait en sorte qu'il y ait un seul fichier
          ->addValidator('Count', false, 1)
          // limite à 2mega
          ->addValidator('Size', false, 2097152)
          ->setMaxFileSize(2097152)
          // seulement des JPEG, PNG, et GIFs
          ->addValidator('Extension', false, 'jpg,png,gif'); 

          $filterChain = new Zend_Filter();
          $filterChain->appendFilter(new Skoch_Filter_File_Resize(array(
          'width' => 60,
          'height' => 60,
          'keepRatio' => true,
          )));
          // Create a medium image with at most 500x200 pixels
          $filterChain->appendFilter(new Skoch_Filter_File_Resize(array(
          'directory' => '../public/inc/account/images/avatar/'.$this->getView()->oUtilisateur['pseudo'],
          'width' => 30,
          'height' => 30,
          'keepRatio' => true,
          )));
         // $image->addFilter('Rename', 'users_upload');
          $image->addFilter($filterChain); 
        $btnValider = new Zend_Form_Element_Submit("valider");

        $btnValider->setAttrib("class", "btn btn-primary");
        $btnValider->setDecorators(array(
            'ViewHelper',
            array(array('wrapperField' => 'HtmlTag'), array('tag' => 'div', 'class' => 'col-lg-offset-2 col-lg-10')),
            array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
        ));
        //Ajout des champs dans l'ordre 
        $this->addElements(array($nom, $prenom, $pseudo, $mail, $addr1, $addr2, $mob, $fixe, $mdp, $ville, $bp,$image, $btnValider));
        $this->setAttrib('enctype', 'multipart/form-data');
    }

}
