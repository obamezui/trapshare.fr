<?php

class Account_Forms_Voyages extends Zend_Form {

    public function init() {
        /**
         * creation du formulaire 
         */
        /* on nomme le formulaire */

        $this->setMethod('post');
        $this->setName("ajoutvoyage :");
        $this->setAttrib("class", "form-validate form-horizontal");
        // Création des éléments du formulaire 
        $depart = new Zend_Form_Element_Text("depart");
        $depart->setLabel('depart  *:')
                ->setRequired(true)
                ->setFilters(array("StripTags", "StringTrim"))
                ->addValidator("notEmpty")
                ->setAttrib("placeholder", "depart")
                ->addErrorMessage("Ce champ est obligatoire")
                ->setAttrib("class", "form-control");
        $depart->setDecorators(array(
            'Errors',
            'ViewHelper',
            array(array('wrapperField' => 'HtmlTag'), array('tag' => 'div', 'class' => 'col-lg-6')),
            array('Label', array('placement' => 'prepend', 'class' => 'col-lg-2 control-label')),
            array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
        ));
        $destination = new Zend_Form_Element_Text("destination");
        $destination->setLabel('destination *:')
                ->setRequired(true)
                ->setFilters(array("StripTags", "StringTrim"))
                ->addValidator("notEmpty")
                ->setAttrib("placeholder", "destination")
                ->addErrorMessage("Ce champ est obligatoire")
                ->setAttrib("class", "form-control");
        $destination->setLabel('destination *:')
                ->setDecorators(array(
                    'Errors',
                    'ViewHelper',
                    array(array('wrapperField' => 'HtmlTag'), array('tag' => 'div', 'class' => 'col-lg-6')),
                    array('Label', array('placement' => 'prepend', 'class' => 'col-lg-2 control-label')),
                    array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
        ));
        $heure = new Zend_Form_Element_Text("heure");
        $heure->setLabel('heure de depart *:')
                ->setRequired(true)
                ->setFilters(array("StripTags", "StringTrim"))
                ->addValidator("notEmpty")
                ->setAttrib("placeholder", "heure de depart")
                ->addErrorMessage("Ce champ est obligatoire")
                ->setAttrib("class", "form-control");
        $heure->setDecorators(array(
            'Errors',
            'ViewHelper',
            array(array('wrapperField' => 'HtmlTag'), array('tag' => 'div', 'class' => 'col-lg-6')),
            array('Label', array('placement' => 'prepend', 'class' => 'col-lg-2 control-label')),
            array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
        ));
        $date = new Zend_Form_Element_Text("date");
        $date->setLabel('date de depart *:')
                ->setRequired(true)
                ->setFilters(array("StripTags", "StringTrim"))
                ->addValidator("notEmpty")
                ->setAttrib("placeholder", "votre date de depart")
                ->addErrorMessage("Ce champ est obligatoire")
                ->setAttrib("class", "form-control");
        $date->setDecorators(array(
            'Errors',
            'ViewHelper',
            array(array('wrapperField' => 'HtmlTag'), array('tag' => 'div', 'class' => 'col-lg-6')),
            array('Label', array('placement' => 'prepend', 'class' => 'col-lg-2 control-label')),
            array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
        ));

        $kilo = new Zend_Form_Element_Text("kilo");
        $kilo->setLabel('kilo proposé(s) *:')
                ->setRequired(true)
                ->setFilters(array("StripTags", "StringTrim"))
                ->addValidator("notEmpty")
                ->setAttrib("placeholder", "kilos partagés")
                ->addErrorMessage("Ce champ est obligatoire")
                ->setAttrib("class", "form-control kilo");
        $kilo->setDecorators(array(
            'Errors',
            'ViewHelper',
            array(array('wrapperField' => 'HtmlTag'), array('tag' => 'div', 'class' => 'col-lg-6')),
            array('Label', array('placement' => 'prepend', 'class' => 'col-lg-2 control-label')),
            array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
        ));
        
         $prix = new Zend_Form_Element_Text("prix");
        $prix->setLabel('prix *:')
                ->setRequired(true)
                ->setFilters(array("StripTags", "StringTrim"))
                ->addValidator("notEmpty")
                ->setAttrib("placeholder", "000.00")
                ->addErrorMessage("Ce champ est obligatoire")
                ->setAttrib("class", "form-control money");
        $prix->setDecorators(array(
            'Errors',
            'ViewHelper',
            array(array('wrapperField' => 'HtmlTag'), array('tag' => 'div', 'class' => 'col-lg-6')),
            array('Label', array('placement' => 'prepend', 'class' => 'col-lg-2 control-label')),
            array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
        ));
        $texte = new Zend_Form_Element_Textarea("texte");
        $texte->setLabel('description :')
                ->setAttrib("class", "form-control")
                ->setRequired(true)
                ->setAttrib("placeholder", "texte de votre annonce")
                ->addErrorMessage("Ce champ est obligatoire")
                ->setAttrib("cols", "50")
                ->setAttrib("rows", "4");
        $texte->setDecorators(array(
            'Errors',
            'ViewHelper',
            array(array('wrapperField' => 'HtmlTag'), array('tag' => 'div', 'class' => 'col-lg-10')),
            array('Label', array('placement' => 'prepend', 'class' => 'col-lg-2 control-label')),
            array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
        ));


        $btnValider = new Zend_Form_Element_Submit("valider");

        $btnValider->setAttrib("class", "btn btn-primary");
        $btnValider->setDecorators(array(
            'ViewHelper',
            array(array('wrapperField' => 'HtmlTag'), array('tag' => 'div', 'class' => 'col-lg-offset-2 col-lg-10')),
            array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
        ));
        //Ajout des champs dans l'ordre 
        $this->addElements(array($depart, $destination, $heure, $date, $kilo,$prix, $texte, $btnValider));
    }

}
