<?php

class Public_Forms_Utilisateur extends Zend_Form {

    public function init() {
        /**
         * creation du formulaire 
         */
        /* on nomme le formulaire */
        $this->setMethod('post');
        $this->setName("inscription :");
        $this->setAttrib("class", "contact-form");
        // Création des éléments du formulaire 
        $nom = new Zend_Form_Element_Text("nom");
        $nom->setLabel('nom *:')
                ->setRequired(true)
                ->setFilters(array("StripTags", "StringTrim"))
                ->addValidator("notEmpty")
                ->setAttrib("placeholder", "votre nom")
                ->addErrorMessage("Ce champ est obligatoire")
                ->setAttrib("class", "form-control");
        $prenom = new Zend_Form_Element_Text("prenom");
        $prenom->setLabel('prenom *:')
                ->setRequired(true)
                ->setFilters(array("StripTags", "StringTrim"))
                ->addValidator("notEmpty")
                ->setAttrib("placeholder", "votre prenom")
                ->addErrorMessage("Ce champ est obligatoire")
                ->setAttrib("class", "form-control");
        $pseudo = new Zend_Form_Element_Text("pseudo");
        $pseudo->setLabel('pseudo *:')
                ->setRequired(true)
                ->setFilters(array("StripTags", "StringTrim"))
                ->addValidator("notEmpty")
                ->setAttrib("placeholder", "votre pseudo")
                ->addErrorMessage("Ce champ est obligatoire")
                ->setAttrib("class", "form-control");
        $mail = new Zend_Form_Element_Text("mail");
        $mail->setLabel('mail *:')
                ->setRequired(true)
                ->setFilters(array("StripTags", "StringTrim"))
                ->addValidator("notEmpty")
                ->setAttrib("placeholder", "votre mail")
                ->addErrorMessage("Ce champ est obligatoire")
                ->setAttrib("class", "form-control");

        $addr1 = new Zend_Form_Element_Text("addr1");
        $addr1->setLabel('addresse 1 *:')
                ->setRequired(true)
                ->setFilters(array("StripTags", "StringTrim"))
                ->addValidator("notEmpty")
                ->setAttrib("placeholder", "address 1")
                ->addErrorMessage("Ce champ est obligatoire")
                ->setAttrib("class", "form-control");
        $addr2 = new Zend_Form_Element_Text("addr2");
        $addr2->setLabel('addresse 2 :')
                ->setAttrib("class", "form-control");
        $mob = new Zend_Form_Element_Text("mob");
        $mob->setLabel('numero mobile :')
                ->setRequired(true)
                ->setFilters(array("StripTags", "StringTrim"))
                ->addValidator("notEmpty")
                ->setAttrib("placeholder", "votre numero mobile")
                ->addErrorMessage("Ce champ est obligatoire")
                ->setAttrib("class", "form-control");
        $fixe = new Zend_Form_Element_Text("fixe");
        $fixe->setLabel('numero fixe  :')
                ->setAttrib("class", "form-control");

        $mdp = new Zend_Form_Element_Password("mdp");
        $mdp->setLabel('mot de passe *:')
                ->setRequired(true)
                ->setFilters(array("StripTags", "StringTrim"))
                ->addValidator("notEmpty")
                ->setAttrib("placeholder", "Votre mot de passe")
                ->addErrorMessage("Ce champ est obligatoire")
                ->setAttrib("class", "form-control");
        $ville = new Zend_Form_Element_Text("ville");
        $ville->setLabel('ville *:')
                ->setRequired(true)
                ->setFilters(array("StripTags", "StringTrim"))
                ->addValidator("notEmpty")
                ->setAttrib("placeholder", "votre ville")
                ->addErrorMessage("Ce champ est obligatoire")
                ->setAttrib("class", "form-control");
        $bp = new Zend_Form_Element_Text("bp");
        $bp->setLabel('Boite Postale *:')
                ->setRequired(true)
                ->setFilters(array("StripTags", "StringTrim"))
                ->addValidator("notEmpty")
                ->setAttrib("placeholder", "votre boite postale")
                ->addErrorMessage("Ce champ est obligatoire")
                ->setAttrib("class", "form-control");
        //$bp->addDecorator('HtmlTag', array('tag' => 'div', 'class' => 'foo'));
     /*   $image = new Zend_Form_Element_File('image');
        $image->setLabel('Image de profil:')
                ->setDestination('../public/inc/account/images/upload/profil')
                // Fait en sorte qu'il y ait un seul fichier
                ->addValidator('Count', false, 1)
                // limite à 2mega
                ->addValidator('Size', false, 2097152)
                ->setMaxFileSize(2097152)
                // seulement des JPEG, PNG, et GIFs
                ->addValidator('Extension', false, 'jpg,png,gif');*/
       /* $image->addFilter(new Skoch_Filter_File_Resize(array(
            'width' => 75,
            'height' => 75,
            'keepRatio' => true,
        )));*/
      /*  $filterChain = new Zend_Filter();
        $filterChain->appendFilter(new Skoch_Filter_File_Resize(array(
            'width' => 75,
            'height' => 75,
            'keepRatio' => true,
        )));
        // Create a medium image with at most 500x200 pixels
       $filterChain->appendFilter(new Skoch_Filter_File_Resize(array(
            'directory' => '../public/inc/account/images/upload/avatar',
            'width' => 35,
            'height' => 35,
            'keepRatio' => true,
        )));
       $image->addFilter('Rename', 'users_upload');
        $image->addFilter($filterChain);*/
        $btnLogin = new Zend_Form_Element_Submit("btnLogin");
        $btnLogin->setLabel('Inscrpition')
                ->setAttrib("class", "btn btn-lg btn-success btn-block");

        //Ajout des champs dans l'ordre 
        $this->addElements(array($nom, $prenom, $pseudo, $mail, $addr1, $addr2, $mob, $fixe, $mdp, $ville, $bp, $btnLogin));
        $this->setAttrib('enctype', 'multipart/form-data');
    }

}
