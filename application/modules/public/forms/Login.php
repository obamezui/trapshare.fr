<?php

class Public_Forms_Login extends Zend_Form
{

    public function init()
    {
        /**
         * creation du formulaire 
         */
         /* on nomme le formulaire */
        $this->setMethod('post');
        $this->setName("login :");
        // Création des éléments du formulaire 
        $login = new Zend_Form_Element_Text("login");
        $login->setLabel('pseudo *:')
                 ->setRequired(true)
                 ->setFilters(array("StripTags","StringTrim"))
                 ->addValidator("notEmpty")
                 ->setAttrib("placeholder","Votre pseudo")
                 ->addErrorMessage("Ce champ est obligatoire")
                 ->setAttrib("class", "form-control");
       
        $mdp = new Zend_Form_Element_Password("mdp");
       $mdp->setLabel('mot de passe :')
                 ->setRequired(true)
                 ->setFilters(array("StripTags","StringTrim"))
                 ->addValidator("notEmpty")
                 ->setAttrib("placeholder","Votre mot de passe")
                 ->addErrorMessage("Ce champ est obligatoire")
                 ->setAttrib("class", "form-control");
        
        $btnLogin = new Zend_Form_Element_Submit("btnLogin");
        $btnLogin->setLabel('Login')
                 ->setAttrib("class","btn btn-lg btn-success btn-block");
        
        //Ajout des champs dans l'ordre 
        $this->addElements(array($login,$mdp,$btnLogin));
    }


}

