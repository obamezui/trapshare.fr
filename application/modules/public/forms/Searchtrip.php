<?php

class Public_Forms_Searchtrip extends Zend_Form
{

    public function init()
    {
          /**
         * creation du formulaire 
         */
         /* on nomme le formulaire */
        $this->setMethod('post');
        $this->setName("tripsearch :");
        // Création des éléments du formulaire 
        $depart = new Zend_Form_Element_Text("depart");
        $depart->setRequired(true)
                ->setFilters(array("StripTags","StringTrim"))
                ->addValidator("notEmpty")
                ->setAttrib("placeholder","depart")
                ->addErrorMessage("Ce champ est obligatoire")
                ->setAttrib("class", "form-control");
       
        $arrivee = new Zend_Form_Element_Text("arrivee");
       $arrivee->setRequired(true)
                ->setFilters(array("StripTags","StringTrim"))
                ->addValidator("notEmpty")
                ->setAttrib("placeholder","arrivee")
                ->addErrorMessage("Ce champ est obligatoire")
                ->setAttrib("class", "form-control");
        
        $search = new Zend_Form_Element_Submit("btnSearch");
        $search->setLabel('rechercher')
                 ->setAttrib("class","btn btn-lg btn-success btn-block");
        
        //Ajout des champs dans l'ordre 
        $this->addElements(array($depart,$arrivee,$search));
    }


}

