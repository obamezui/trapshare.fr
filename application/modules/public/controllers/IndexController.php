<?php

class Public_IndexController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_helper->layout->setLayout('public');
    }

    public function indexAction()
    {
        $this->view->displaySearch=false;
        $searchform = new Public_Forms_Searchtrip();
        $this->view->searchform = $searchform;
          
        
        if($this->getRequest()->isPost()){
             $donnees = $this->getRequest()->getPost();
           if($searchform->isValid($donnees))
           {
               $this->view->displaySearch=true;
            $this->oModele = new Account_Models_DbTable_Voyages();
            $this->view->voyages = $this->oModele->recupererVoyages($searchform->getValue("depart"),$searchform->getValue("arrivee"));
           }else{
               
               $searchform->populate($donnees);
           }
           
            
        }
    }
    


}

