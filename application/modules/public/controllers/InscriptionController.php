<?php

class Public_InscriptionController extends Zend_Controller_Action {

    public function init() {
        $this->_helper->layout->setLayout('public');
        $this->view->headTitle('inscription');
    }

    public function indexAction() {
        // initialisation du formulaire article

        $inscriptionForm = new Public_Forms_Utilisateur();
        $this->view->inscriptionForm = $inscriptionForm;
        if ($this->getRequest()->isPost()) {
            //Récupération des donnnées
            $oDonnees = $this->getRequest()->getPost();
            if ($inscriptionForm->isValid($oDonnees)) {
                //Déclencher l'ajout des données 
                $this->oModele = new Public_Models_DbTable_Utilisateur();
                /*if ($inscriptionForm->image->isUploaded()) {
                    //on recupere les infos sur le fichier envoyé
                    $uploadedData = $inscriptionForm->getValues();
                    $transferAdapter = new Zend_File_Transfer_Adapter_Http();
                    $transferAdapter->receive();
                    var_dump($transferAdapter->getFileInfo());
                }*/
                 $rs = $this->oModele->ajouter(
                  array(
                  "id" => new Zend_Db_Expr('UUID()') ,
                  "nom" => $inscriptionForm->getValue("nom"),
                  "prenom" => $inscriptionForm->getValue("prenom"),
                  "pseudo" => $inscriptionForm->getValue("pseudo"),
                  "mdp" => $inscriptionForm->getValue("mdp"),
                  "mdp" => md5($inscriptionForm->getValue("mdp")),
                  "role" => "0",
                  "adresse2" => $inscriptionForm->getValue("addr2"),
                  "mobile" => $inscriptionForm->getValue("mob"),
                  "fixe" => $inscriptionForm->getValue("fixe"),
                  "email" => $inscriptionForm->getValue("mail"),
                  "ville" => $inscriptionForm->getValue("ville"),
                  "bp" => $inscriptionForm->getValue("bp"),
                  "picture" =>"null"

                  )); 
                 //url pour l'image du profil 
                 $profil = '../public/inc/account/images/profil/'.$inscriptionForm->getValue("pseudo");
                 $avatar = '../public/inc/account/images/avatar/'.$inscriptionForm->getValue("pseudo");
                 //creation du repertoire du profil
                 mkdir($profil);
                 mkdir($avatar);
            } else {
                $inscriptionForm->populate($oDonnees);
            }
        }
    }

}
