<?php

class Public_Models_DbTable_Utilisateur extends Zend_Db_Table_Abstract
{

    protected $_name = 'utilisateur';
    /*
     * fonction pour ajouter un utilisateur
     */
    public function ajouter($dataUser){
        $this->insert($dataUser);
        
    }
    
     /**
     * 
     * @param type $pIntId id de l'utilisateur
     * @return l'utilisateur
     */
       public function recuperer($login,$mdp){
        return $this->fetchRow(array("pseudo=?"=>$login,"mdp=?"=>md5($mdp)));
          
    }
    
    public function recupererinfos($userid){
        return $this->fetchRow(array("id=?"=>$userid));
          
    }
    
      public function supprimer ($userId){
        $this->delete('id='.(int)$userId);
        
    }


}

