<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	  protected function _initAutoLoader() {
        // Astuce permettant l'utilisation des classes
        // placées dans les modules :
        $oRessources = new Zend_Loader_Autoloader_Resource(
        array(
            'namespace' => '',
            'basePath' => APPLICATION_PATH,
            'resourceTypes' => array(

                'module_public' => array(
                'path' => 'modules/public/',
                'namespace' => 'Public'),

                'module_cockpit' => array(
                'path' => 'modules/cockpit/',
                'namespace' => 'Cockpit'
                ),
                 'module_account' => array(
                'path' => 'modules/account/',
                'namespace' => 'Account'
                )
                
                )));
    
         return $oRessources;
    }
    /**
     * fonction pour gerer les logins
     */
    protected  function  _initSecurisation(){
        // j'instancie la class sécurisation pour vérifier si l'utilisateur est
        //connecté 
        $oSecurisation = new App_Plugins_Securisation();
        Zend_Controller_Front::getInstance()->registerPlugin($oSecurisation);
        
        
    }
    
     /*protected  function  _initRedirection(){
        // j'instancie la class sécurisation pour vérifier si l'utilisateur est
        //connecté 
        $oRedirection = new App_Plugins_Redirection();
        Zend_Controller_Front::getInstance()->registerPlugin($oRedirection);
        
        
    }*/
    
     
    

}

