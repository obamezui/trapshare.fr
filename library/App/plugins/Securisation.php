<?php

class App_Plugins_Securisation extends Zend_Controller_Plugin_Abstract {
    
    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $pRequest) {
        //Récupération du nom du module :
        if ($pRequest->getModuleName()== 'account') {
            //on limite la durée de la session
          // Zend_Session::RememberMe(3600);
           //initialization de session 
            $oSessionSecurite = new Zend_Session_Namespace('securite');
            if ($oSessionSecurite->laisserPasser !=true) {
                //l'utilisateur n'est pas loggé ,on effectue une redirection
                $pRequest->setModuleName("account");
                $pRequest->setControllerName("Index");
                 $pRequest->setActionName("login");
                
            }
        }
    }
}

