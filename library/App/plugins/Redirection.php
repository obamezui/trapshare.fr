<?php

/**
 * cette classe gere les erreurs lors de la saisie des url
 *
 * @author mezui
 */

class App_Plugins_Redirection extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        // on recupere une instance du controleur frontal 
         $front = Zend_Controller_Front::getInstance();
         //on recupere une instance du dispatcher
        $dispatcher = $front->getDispatcher();
        //on teste si cette action est valide 
         if($dispatcher->isDispatchable($request)) {
             
        } else {
            $request->setModuleName("public");
            $request->setControllerName("Error");
            $request->setActionName("error");
        }
    }
}
